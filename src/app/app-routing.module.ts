import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AccountRegistrationComponent } from './account-registration/account-registration.component';
import { ThankYouPageComponent } from './thank-you-page/thank-you-page.component';

const routes: Routes = [
  { path: '', redirectTo: 'account-registration', pathMatch: 'full' },
  {
    path: 'account-registration',
    component: AccountRegistrationComponent,
  },
  {
    path: 'thank-you',
    component: ThankYouPageComponent
  },
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
