import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbModalRef, NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-account-registration',
  templateUrl: './account-registration.component.html',
  styleUrls: ['./account-registration.component.scss']
})
export class AccountRegistrationComponent implements OnInit {
  public showErr: boolean =false;
  public modalRef: NgbModalRef;
  
  @ViewChild('confirmFrom') confirmFrom: ElementRef;
  constructor(
    private fb: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private modalService: NgbModal,
  ) { }

  accountForm = this.fb.group({
    fName: ['', Validators.compose([Validators.required,Validators.minLength(1), Validators.maxLength(255)])],
    lName: ['', Validators.compose([Validators.required,Validators.minLength(1), Validators.maxLength(255)])],
    phone: ['', Validators.compose([Validators.required,Validators.minLength(1), Validators.maxLength(10)])],
    email: ['', Validators.compose([Validators.required, Validators.pattern(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)])],
    ext:['']
  });

  ngOnInit(): void {
  }
  
  next(){
    if (!this.accountForm.valid) {
      this.showErr = true;
      return;
    }
    this.showErr = false;
    this.modalRef = this.modalService.open(this.confirmFrom, { centered: true, size: 'xsm', backdrop: "static" });
  }

  dis() {
    setTimeout(() => this.modalRef.close(), 300);
  }

  confirm(){
    this.dis()
    this.router.navigate(['thank-you'])
  }
}
